<?php declare(strict_types=1);

use Lubiana\CodeQuality\LubiSetList;
use PhpCsFixer\Fixer\ClassNotation\FinalClassFixer;
use Symplify\EasyCodingStandard\Config\ECSConfig;

return ECSConfig::configure()
    ->withPaths(require __DIR__ . '/code-quality-paths.php')
    ->withRootFiles()
    ->withRules([FinalClassFixer::class])
    ->withSets([LubiSetList::ECS]);
