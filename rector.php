<?php declare(strict_types=1);

use Lubiana\CodeQuality\LubiSetList;
use Rector\CodingStyle\Rector\Closure\StaticClosureRector;
use Rector\Config\RectorConfig;

return RectorConfig::configure()
    ->withPaths(require __DIR__ . '/code-quality-paths.php')
    ->withSkip([
        StaticClosureRector::class => [__DIR__ . '/tests'],
    ])
    ->withSets([LubiSetList::RECTOR]);
